﻿using System.Threading.Tasks;
using Application.Interfaces;
using Microsoft.Extensions.Options;
using Stripe;

namespace Infrastructure.Payment
{
    public class PaymentAccessor : IPaymentAccessor
    {
        private readonly IOptions<StripeSettings> _config;

        public PaymentAccessor(IOptions<StripeSettings> config)
        {
            _config = config;
        }

        public async Task<string> CreatePayment(string stripeToken, decimal amount)
        {
            var customers = new CustomerService();
            var charges = new ChargeService();

            var customer = await customers.CreateAsync(new CustomerCreateOptions
            {
                Source = stripeToken
            });

            var charge = await charges.CreateAsync(new ChargeCreateOptions
            {
                Amount = (long)amount * 100,
                Description = "Vinyl Shop Purchase",
                Currency = "pln",
                Customer = customer.Id
            });

            return charge.Id;
        }
    }
}