﻿namespace Domain.Infrastructure
{
    public static class DecimalExtensions
    {
        public static string GetValueString(this decimal value)
        {
            return value.ToString("C");
        }
    }
}