﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class StockOnHold
    {
        [Key]
        public int StockOnHoldId { get; set; }
        public string SessionId { get; set; }

        public int Quantity { get; set; }
        public DateTime ExpiryDate { get; set; }

        public int StockId { get; set; }
        public Stock Stock { get; set; }
    }
}