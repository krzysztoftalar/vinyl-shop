﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Order
    {
        
        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public string OrderRef { get; set; }
        public string StripeReference { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<OrderStock> OrderStocks { get; set; }
    }
}