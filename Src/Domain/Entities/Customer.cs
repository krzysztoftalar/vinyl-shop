﻿namespace Domain.Entities
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string AppUserId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public string Street { get; set; }
        public string HomeNumber { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }

        public virtual AppUser AppUser { get; set; }
        public virtual Order Order { get; set; }
    }
}