﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Stock
    {
        public int StockId { get; set; }
        public int ProductId { get; set; }

        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string ProductType { get; set; }

        public virtual Product Product { get; set; }
        public virtual StockOnHold StockOnHold { get; set; }
        public virtual ICollection<OrderStock> OrderStocks { get; set; }
    }
}