﻿import React, {useState} from 'react';
import {observer} from "mobx-react-lite"
import CustomerInformation from "./CustomerInformation"
import Tab from "../../components/Tab"
import CustomerOrders from "./CustomerOrders"

const tabs = [
    {menuItem: "Information", component: <CustomerInformation/>},
    {menuItem: "Orders", component: <CustomerOrders/>},
]

const CustomerDashboard = () => {
    const [showComponent, setShowComponent] = useState(false)
    const [activeComponent, setActiveComponent] = useState<{ menuItem: string, component: JSX.Element }>()

    const changeActiveTab = (menuItem: string) => {
        setShowComponent(true)
        setActiveComponent(tabs.find(x => x.menuItem === menuItem))
    }

    return (
        <div className="section">
            <div className="tabs is-boxed">
                <ul>
                    {tabs.map(tab => {
                        return (
                            <Tab
                                tab={tab}
                                key={tab.menuItem}
                                changeActiveTab={changeActiveTab}
                            />
                        )
                    })}
                </ul>
            </div>
            {showComponent && activeComponent!.component}
        </div>
    );
};

export default observer(CustomerDashboard);