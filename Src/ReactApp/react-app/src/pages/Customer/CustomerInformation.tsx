﻿import React, {useContext, useState} from 'react';
import {observer} from "mobx-react-lite"
import {RootStoreContext} from "../../app/stores/rootStore"
import {CustomerFormValues} from "../../app/models/customer"

const CustomerInformation = () => {
    const rootStore = useContext(RootStoreContext)
    const {createCustomer} = rootStore.customerStore
    const [customer, setCustomer] = useState(new CustomerFormValues());

    const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
        const {name, value} = event.currentTarget
        setCustomer({
            ...customer,
            [name]: value
        })
    }

    const handleSubmit = async (event: React.SyntheticEvent) => {
        event.preventDefault()
        await createCustomer(customer)
    }

    return (
        <form onSubmit={e => handleSubmit(e)}>
            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">First Name</label>
                </div>
                <div className="field-body">
                    <div className="field">
                        <input
                            onChange={(e) => handleChange(e)}
                            value={customer.firstName}
                            name="firstName"
                            required
                            className="input"
                            type="text"
                            placeholder="First Name"/>
                    </div>
                </div>
            </div>
            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">Last Name</label>
                </div>
                <div className="field-body">
                    <div className="field">
                        <input
                            onChange={(e) => handleChange(e)}
                            value={customer.lastName}
                            name="lastName"
                            required
                            className="input"
                            type="text"
                            placeholder="Last Name"/>
                    </div>
                </div>
            </div>
            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">Email</label>
                </div>
                <div className="field-body">
                    <div className="field">
                        <input
                            onChange={(e) => handleChange(e)}
                            value={customer.email}
                            name="email"
                            required
                            className="input"
                            type="email"
                            placeholder="Email"/>
                    </div>
                </div>
            </div>
            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">Phone Number</label>
                </div>
                <div className="field-body">
                    <div className="field">
                        <input
                            onChange={(e) => handleChange(e)}
                            value={customer.phoneNumber}
                            name="phoneNumber"
                            required
                            className="input"
                            type="text"
                            placeholder="Phone Number"/>
                    </div>
                </div>
            </div>
            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">Street</label>
                </div>
                <div className="field-body">
                    <div className="field">
                        <input
                            onChange={(e) => handleChange(e)}
                            value={customer.street}
                            name="street"
                            required
                            className="input"
                            type="text"
                            placeholder="Street"/>
                    </div>
                </div>
            </div>
            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">Home Number</label>
                </div>
                <div className="field-body">
                    <div className="field">
                        <input
                            onChange={(e) => handleChange(e)}
                            value={customer.homeNumber}
                            name="homeNumber"
                            required
                            className="input"
                            type="text"
                            placeholder="Home Number"/>
                    </div>
                </div>
            </div>
            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">City</label>
                </div>
                <div className="field-body">
                    <div className="field">
                        <input
                            onChange={(e) => handleChange(e)}
                            value={customer.city}
                            name="city"
                            required
                            className="input"
                            type="text"
                            placeholder="City"/>
                    </div>
                </div>
            </div>
            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">Post Code</label>
                </div>
                <div className="field-body">
                    <div className="field">
                        <input
                            onChange={(e) => handleChange(e)}
                            value={customer.postCode}
                            name="postCode"
                            required
                            className="input"
                            type="text"
                            placeholder="Post Code"/>
                    </div>
                </div>
            </div>
            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">Country</label>
                </div>
                <div className="field-body">
                    <div className="field">
                        <input
                            onChange={(e) => handleChange(e)}
                            value={customer.country}
                            name="country"
                            required
                            className="input"
                            type="text"
                            placeholder="Country"/>
                    </div>
                </div>
            </div>
            <div className="field is-horizontal">
                <div className="field-label" />             
                <div className="field-body">
                    <div className="field">
                        <div className="control">
                            <button type="submit" className="button is-primary">
                                Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    );
};

export default observer(CustomerInformation);