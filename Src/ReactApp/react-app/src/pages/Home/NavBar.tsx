import React, {useContext, useEffect} from "react"
import {observer} from "mobx-react-lite"
import {Link} from "react-router-dom"
import LoginForm from "../../pages/User/LoginForm"
import {RootStoreContext} from "../../app/stores/rootStore"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faAngleDown, faShoppingCart} from "@fortawesome/free-solid-svg-icons"

const NavBar = () => {
    const rootStore = useContext(RootStoreContext);
    const {openModal} = rootStore.modalStore
    const {user, isLoggedIn, logout} = rootStore.userStore
    const {totalValue, getCart} = rootStore.cartStore

    const token = window.localStorage.getItem("jwt")

    useEffect(() => {
        getCart()
    }, [getCart])

    document.addEventListener("DOMContentLoaded", () => {
        const navbarBurgers = Array.prototype.slice.call(
            document.querySelectorAll(".navbar-burger"),
            0
        )
        if (navbarBurgers.length > 0) {
            navbarBurgers.forEach((el) => {
                el.addEventListener("click", () => {
                    const target = el.dataset.target
                    const target2 = document.getElementById(target)
                    el.classList.toggle("is-active")
                    target2?.classList.toggle("is-active")
                })
            })
        }
    })

    return (
        <nav className="navbar" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <Link to="/" className="navbar-item">
                    <img
                        src="https://bulma.io/images/bulma-logo.png"
                        width="112"
                        height="28"
                        alt=""
                    />
                </Link>
                <div
                    role="button"
                    className="navbar-burger burger"
                    aria-label="menu"
                    aria-expanded="false"
                    data-target="navbarBasicExample"
                >
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </div>
            </div>
            <div id="navbarBasicExample" className="navbar-menu">
                <div className="navbar-start">
                    <Link to="/admin" className="navbar-item">
                        Admin
                    </Link>
                </div>
                <div className="navbar-end">
                    <div className="navbar-item field is-grouped">
                        <p className="control">
                            <Link to="/carts" className="button">                                        
                                <span className="icon">
                                    <FontAwesomeIcon icon={faShoppingCart}/>
                                </span>
                                <span>
                                    {`${totalValue} zł`}
                                </span>
                            </Link>
                        </p>
                        {isLoggedIn && user && token ? (
                            <div className="dropdown is-hoverable">
                                <div className="dropdown-trigger">
                                    <button className="button" aria-haspopup="true" aria-controls="dropdown-menu3">
                                        <span>{user.displayName}</span>
                                        <span className="icon is-small">
                                        <FontAwesomeIcon icon={faAngleDown}/>
                                  </span>
                                    </button>
                                </div>                               
                                <div className="dropdown-menu" id="dropdown-menu3" role="menu">
                                    <div className="dropdown-content">
                                        <Link to="/customers" className="dropdown-item">
                                            My Account
                                        </Link>
                                        <hr className="dropdown-divider" />
                                        <a onClick={() => logout()} className="dropdown-item">
                                            Logout
                                        </a>
                                    </div>
                                </div>
                            </div>
                        ) : (
                            <div>
                                <div onClick={() => openModal(<LoginForm/>)} className="button">
                                    Sign In
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default observer(NavBar)
