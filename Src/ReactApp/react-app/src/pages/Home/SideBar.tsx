﻿import React, {useContext, useEffect} from 'react';
import {observer} from "mobx-react-lite"
import {RootStoreContext} from "../../app/stores/rootStore"

const SideBar = () => {
    const rootStore = useContext(RootStoreContext)
    const {loadCategories, categories} = rootStore.categoryStore
    const {setPredicate} = rootStore.productStore

    useEffect(() => {
        loadCategories()
    }, [loadCategories]);

    return (
        <div className="column is-2">
            <aside className="menu">
                <p className="menu-label">
                    Categories
                </p>
                <ul className="menu-list">
                    <li>
                        <a onClick={() => setPredicate("all", "true")}>
                            All
                        </a>
                    </li>
                </ul>
                {categories.map(category => (
                    <ul className="menu-list"
                        key={category.categoryId}>
                        <li>
                            <a onClick={() => setPredicate("categoryName", category.categoryName)}>
                                {category.categoryName}
                            </a>
                        </li>
                    </ul>
                ))}
            </aside>
        </div>
    );
};

export default observer(SideBar);