﻿import React from 'react';
import {observer} from "mobx-react-lite"
import ProductDashboard from "../Product/ProductDashboard"
import SideBar from "./SideBar"

const HomeDashboard = () => {
    return (
        <div className="section">
            <div className="columns">
                <SideBar/>
                <ProductDashboard/>
            </div>
        </div>
    );
};

export default observer(HomeDashboard);