﻿import React, {useContext} from 'react';
import {useElements, useStripe, CardElement} from "@stripe/react-stripe-js"
import CardSection from "./CardSection"
import {StripeCardElement, Token} from "@stripe/stripe-js"
import {RootStoreContext} from "../../app/stores/rootStore"

const CheckoutForm = () => {
    const rootStore = useContext(RootStoreContext)
    const {createPayment} = rootStore.checkoutStore

    const stripe = useStripe();
    const elements = useElements();

    const handleSubmit = async (event: React.SyntheticEvent) => {
        event.preventDefault();

        if (!stripe || !elements) {
            return;
        }

        if (stripe) {
            let card: StripeCardElement | null = elements.getElement(CardElement)
            let token: { token?: Token } = await stripe.createToken(card!)           
            await createPayment(token.token!.id)
        }
    }
    
    return (
        <form onSubmit={handleSubmit}>
            <CardSection/>
            <button type="submit" disabled={!stripe}>Submit Payment</button>
        </form>
    )
}

export default CheckoutForm;