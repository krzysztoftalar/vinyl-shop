﻿import React from 'react';
import {observer} from "mobx-react-lite"
import {Elements} from "@stripe/react-stripe-js";
import {loadStripe} from '@stripe/stripe-js';
import CheckoutForm from "./CheckoutForm"

const CheckoutDashboard = () => {
    const stripePromise = loadStripe("");
    
    return (
        <Elements stripe={stripePromise}>
            <CheckoutForm />
        </Elements>
    );
};

export default observer(CheckoutDashboard);