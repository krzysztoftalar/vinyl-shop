import React, {useContext, useEffect, useState} from "react"
import {observer} from "mobx-react-lite"
import {RootStoreContext} from "../../app/stores/rootStore"
import {RouteComponentProps} from "react-router-dom"

interface DetailParams {
    id: string
}

const ProductDetails: React.FC<RouteComponentProps<DetailParams>> = ({match, history}) => {
    const rootStore = useContext(RootStoreContext)
    const {product, loadProduct} = rootStore.productStore
    const {addToCart} = rootStore.cartStore

    const [cart, setCart] = useState<{ stockId: number, quantity: number }>({stockId: 0, quantity: 0});
    const [maxQty, setMaxQty] = useState(10);
    const [price, setPrice] = useState(product && product.stock.length > 0 ? product.stock[0].price : "");

    useEffect(() => {
        loadProduct(parseInt(match.params.id))
    }, [loadProduct, match.params.id, history])

    useEffect(() => {
        if (cart.stockId !== 0 && product) {
            setMaxQty(product.stock.find(x => x.stockId == cart.stockId)!.quantity)
            setPrice(product.stock.find(x => x.stockId == cart.stockId)!.price)
        }
    }, [cart, product])

    const handleSelectChange = (event: React.FormEvent<HTMLSelectElement>) => {
        const {name, value} = event.currentTarget
        setCart(prev => {
            if (value != "") {
                return {
                    ...prev,
                    [name]: value
                }
            }
            return prev
        })
    }

    const handleQuantityChange = (event: React.FormEvent<HTMLInputElement>) => {
        const {name, value} = event.currentTarget
        setCart(prev => ({...prev, [name]: value}))
    }

    const handleSubmit = async (event: React.SyntheticEvent) => {
        event.preventDefault()
        await addToCart(cart.stockId, cart.quantity)
    }

    if (!product) return <h1>Product not found</h1>

    return (
        <div className="hero">
            <div className="hero-body">
                <div className="container">
                    <div className="columns">
                        <div className="column is-6">
                            <figure className="image">
                                <img src="/assets/Duran-Duran-Rio.jpg" alt=""/>
                            </figure>
                        </div>
                        <div className="column is-6">
                            <section className="content">
                                <p className="title">{product.productName}</p>
                                <p className="subtitle">{product.description}</p>
                                <p className="subtitle">{price}</p>
                                <footer>
                                    <form onSubmit={e => handleSubmit(e)}>
                                        <div className="field is-horizontal">
                                            <div className="field-label is-normal">
                                                <label className="label">Type</label>
                                            </div>
                                            <div className="field-body">
                                                <div className="field is-narrow">
                                                    <div className="control">
                                                        <div className="select is-fullwidth">
                                                            <select
                                                                onChange={e => handleSelectChange(e)}
                                                                value={cart.stockId}
                                                                name="stockId"
                                                                required
                                                            >
                                                                <option value="">-- Please Choose a type --</option>

                                                                {product && product.stock.map((stock) => {
                                                                    const disabled: boolean = stock.quantity <= 0

                                                                    return (
                                                                        <option key={stock.stockId}
                                                                                disabled={disabled}
                                                                                value={stock.stockId}>
                                                                            {stock.productType}
                                                                        </option>
                                                                    )
                                                                })}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="field is-horizontal">
                                            <div className="field-label is-normal">
                                                <label className="label">Quantity</label>
                                            </div>
                                            <div className="field-body">
                                                <div className="field is-narrow">
                                                    <div className="control">
                                                        <input
                                                            onChange={e => handleQuantityChange(e)}
                                                            value={cart.quantity}
                                                            name="quantity"
                                                            className="input"
                                                            type="number"
                                                            min="1"
                                                            max={maxQty}
                                                            required
                                                            pattern="\d*"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="field is-horizontal">
                                            <div className="field-label is-normal"/>
                                            <div className="field-body">
                                                <div className="field is-narrow">
                                                    <div className="control">
                                                        <button className="button is-success" type="submit">
                                                            Add To Cart
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </footer>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default observer(ProductDetails)
