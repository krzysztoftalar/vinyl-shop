﻿import React, {useContext, useEffect, useState} from 'react';
import {observer} from "mobx-react-lite"
import {RootStoreContext} from "../../app/stores/rootStore"
import ProductList from "./ProductList"
import InfiniteScroll from "react-infinite-scroller"

const ProductDashboard = () => {
    const rootStore = useContext(RootStoreContext);
    const {loadProducts, setPage, pageIndex, totalPages} = rootStore.productStore
    const [loadnigNext, setLoadnigNext] = useState(false)

    useEffect(() => {
        loadProducts()
    }, [loadProducts])

    const handleGetNext = () => {
        setPage(pageIndex + 1)
        loadProducts().then(() => setLoadnigNext(false))
    }

    return (
        <div>
            <InfiniteScroll
                pageStart={0}
                loadMore={handleGetNext}
                hasMore={!loadnigNext && pageIndex + 1 < totalPages}
                initialLoad={false}
            >
                <ProductList/>
            </InfiniteScroll>
        </div>
    );
};

export default observer(ProductDashboard);