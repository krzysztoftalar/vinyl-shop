import React, {useContext} from "react"
import {RootStoreContext} from "../../app/stores/rootStore"
import {observer} from "mobx-react-lite"
import ProductListItem from "./ProductListItem"

const ProductsList = () => {
    const rootStore = useContext(RootStoreContext)
    const {products} = rootStore.productStore

    return (
        <div className="column is-12">
            <div className="columns is-multiline is-mobile">
                {products.map((product, index) => {
                    return <ProductListItem key={index} product={product}/>
                })}
            </div>
        </div>
    )
}

export default observer(ProductsList)
