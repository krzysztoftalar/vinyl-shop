import React from "react"
import {IProduct} from "../../app/models/product"
import {observer} from "mobx-react-lite"
import {Link} from "react-router-dom"

const ProductListItem: React.FC<{ product: IProduct }> = ({product}) => {
    const price = product.stock.length > 0 ? product.stock[0].price : "Out of Stock"
    
    return (
        <div className="column is-narrow">
            <Link to={`/products/${product.productId}`}>
                <div className="card">
                    <div className="card-image">
                        <figure className="image is-square">
                            <img src="/assets/Duran-Duran-Rio.jpg" alt=""/>
                        </figure>
                    </div>
                    <div className="card-content">
                        <p className="title is-size-5">
                            {product.productName}
                        </p>
                        <p className="subtitle">{price}</p>
                    </div>
                </div>
            </Link>
        </div>
    )
}

export default observer(ProductListItem)
