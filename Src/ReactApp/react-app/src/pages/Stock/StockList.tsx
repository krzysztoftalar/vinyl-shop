﻿import React, {useContext, useEffect, useState} from 'react';
import {observer} from "mobx-react-lite"
import CreateStock from "./CreateStock"
import {RootStoreContext} from "../../app/stores/rootStore"
import {IStock} from "../../app/models/stock"

const StockList: React.FC = () => {
    const rootStore = useContext(RootStoreContext);
    const {deleteStock, updateStock, stockProduct, submitting} = rootStore.stockStore
    const {openModal} = rootStore.modalStore

    const [stock, setStock] = useState<IStock[]>([]);

    useEffect(() => {
        setStock(stockProduct!.stock)
    }, [stockProduct, stock]);

    const handleChange = (index: number, event: React.FormEvent<HTMLInputElement>) => {
        const {name, value} = event.currentTarget

        if (stock) {
            setStock(prevStock => prevStock.fill(
                {
                    ...prevStock[index],
                    [name]: name === ("price" || "quantity") ? parseInt(value) : value,               
                    productId: stockProduct!.productId,
                }, index, index + 1
            ))
        }
    }

    return (
        <div className="column is-5">
            <nav className="level">
                <div className="level-left">
                    <div className="level-item">
                        <h2 className="title">
                            Stock List
                        </h2>
                    </div>
                </div>
                <div className="level-right">
                    <p className="level-item">
                        <button onClick={() => openModal(<CreateStock/>)}
                                className="button is-primary is-outlined">
                            Add Stock
                        </button>
                    </p>
                </div>
            </nav>
            <table className="table">
                <thead>
                <tr>
                    <th>Product Type</th>
                    <th>Quantity</th>
                    <th style={{width: "100px"}}>Price</th>
                </tr>
                </thead>
                <tbody>
                {stockProduct && stockProduct.stock.map((s, index) => {
                    return (
                        <tr key={s.stockId}>
                            <td>
                                <input
                                    onChange={e => handleChange(index, e)}
                                    value={s.productType}
                                    name="productType"
                                    className="input"/>
                            </td>
                            <td>
                                <input
                                    onChange={e => handleChange(index, e)}
                                    value={s.quantity}
                                    name="quantity"
                                    className="input"/>
                            </td>
                            <td>
                                <input
                                    onChange={e => handleChange(index, e)}
                                    value={s.price}
                                    name="price"
                                    className="input"/>
                            </td>
                            <td>
                                <button onClick={e => deleteStock(s.stockId, e)}
                                        name={s.stockId.toString()}
                                        className="button has-text-danger">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
            <button disabled={stockProduct!.stock.length === 0}
                    onClick={e => updateStock(stock, e)}
                    className={`button is-primary is-outlined ${submitting ? `is-loading` : ``}`}>
                Update Stock
            </button>
        </div>
    );
};

export default observer(StockList);