﻿import React, {useContext, useState} from 'react';
import {observer} from "mobx-react-lite"
import {RootStoreContext} from "../../app/stores/rootStore"
import {StockFormValues} from "../../app/models/stock"

const CreateStock: React.FC = () => {
    const rootStore = useContext(RootStoreContext);
    const {createStock, stockProduct} = rootStore.stockStore

    const [newStock, setNewStock] = useState(new StockFormValues());
    const {closeModal} = rootStore.modalStore

    const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
        const {name, value} = event.currentTarget
         setNewStock({
            ...newStock,
            [name]: value,
            productId: stockProduct!.productId
        })
    }

    const handleSubmit = async (event: React.SyntheticEvent) => {
        event.preventDefault()
        await createStock(newStock).then(closeModal)
        setNewStock(new StockFormValues())
    }

    return (
        <div className="modal is-active">
            <div className="modal-background"/>
            <div className="modal-card">
                <header className="modal-card-head">
                    <p className="modal-card-title">New Stock</p>
                    <button onClick={closeModal} className="delete" aria-label="close"/>
                </header>
                <section className="modal-card-body">
                    <form onSubmit={handleSubmit}>
                        <div className="field">
                            <p className="control">
                                <label className="label">Product Type</label>
                                <input
                                    onChange={e => handleChange(e)}
                                    value={newStock.productType}
                                    name="productType"
                                    className="input"/>
                            </p>
                        </div>
                        <div className="field">
                            <p className="control">
                                <label className="label">Quantity</label>
                                <input
                                    onChange={e => handleChange(e)}
                                    value={newStock.quantity}
                                    name="quantity"
                                    type="number"
                                    className="input"/>
                            </p>
                        </div>
                        <div className="field">
                            <p className="control">
                                <label className="label">Price</label>
                                <input
                                    onChange={e => handleChange(e)}
                                    value={newStock.price}
                                    name="price"
                                    className="input"/>
                            </p>
                        </div>
                        <footer className="modal-card-foot">
                            <button type="submit" className="button is-success">Save changes</button>
                            <button onClick={closeModal} className="button">Cancel</button>
                        </footer>
                    </form>
                </section>
            </div>
        </div>
    );
};

export default observer(CreateStock);