﻿import React, {useContext, useEffect, useState} from 'react';
import {observer} from "mobx-react-lite"
import {RootStoreContext} from "../../app/stores/rootStore"
import StockList from "./StockList"

const StockDashboard = () => {
    const rootStore = useContext(RootStoreContext);
    const {loadStock, stockProducts, setActiveProduct} = rootStore.stockStore
    const [showComponent, setShowComponent] = useState(false)

    useEffect(() => {
        loadStock()
    }, [loadStock])

    return (
        <div className="column is-12 is-multiline is-mobile">
            <div className="columns">
                <div className="column is-5">
                    <aside className="menu">
                        <p className="menu-label">
                            Products
                        </p>
                        {stockProducts && stockProducts.map(product => (
                            <ul className="menu-list"
                                key={product.productId}>
                                <li>
                                    <a onClick={() => {
                                        setActiveProduct(product.productId);
                                        setShowComponent(true)
                                    }}>
                                        {product.productName}
                                    </a>
                                </li>
                            </ul>
                        ))}
                    </aside>
                </div>
                {showComponent && <StockList/>}
            </div>
        </div>
    );
};

export default observer(StockDashboard);