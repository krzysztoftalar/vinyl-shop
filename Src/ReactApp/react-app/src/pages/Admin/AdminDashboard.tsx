﻿import React, {useState} from 'react';
import {observer} from "mobx-react-lite"
import StockDashboard from "../Stock/StockDashboard"

const panel = [
    {menuItem: "Stock", component: <StockDashboard/>}
]

const AdminDashboard = () => {
    const [showComponent, setShowComponent] = useState(false)
    const [activeComponent, setActiveComponent] = useState<{ menuItem: string, component: JSX.Element }>()

    const handleClick = (menuItem: string) => {
        setShowComponent(true)
        setActiveComponent(panel.find(x => x.menuItem === menuItem))
    }

    return (
        <div className="section">
            <div className="columns">
                <div className="column is-2">
                    <aside className="menu">
                        <p className="menu-label">
                            Store Menu
                        </p>
                        {panel.map(item => {
                            return (
                                <ul key={item.menuItem} className="menu-list">
                                    <li>
                                        <a onClick={() => handleClick(item.menuItem)}>
                                            {item.menuItem}
                                        </a>
                                    </li>
                                </ul>
                            )
                        })}
                    </aside>
                </div>
                {showComponent && (activeComponent!.component)}
            </div>
        </div>
    );
};

export default observer(AdminDashboard);