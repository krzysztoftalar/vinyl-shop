﻿import React, {useContext, useState} from 'react';
import {observer} from "mobx-react-lite"
import {RootStoreContext} from "../../app/stores/rootStore"
import {UserFormValues} from "../../app/models/user"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faEnvelope, faLock, faSignInAlt, faUserTie, faUser} from "@fortawesome/free-solid-svg-icons"
import LoginForm from "./LoginForm"

const RegisterForm = () => {
    const rootStore = useContext(RootStoreContext);
    const {closeModal, openModal} = rootStore.modalStore
    const {register} = rootStore.userStore

    const [user, setUser] = useState(new UserFormValues());

    const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
        const {name, value} = event.currentTarget
        setUser({
            ...user,
            [name]: value
        })
    }

    const handleSubmit = async (event: React.SyntheticEvent) => {
        event.preventDefault()
        await register(user)
    }

    return (
        <div className="modal is-active">
            <div className="modal-background"/>
            <div className="modal-card has-text-centered">
                <header className="modal-card-head ">
                    <p className="modal-card-title">Sign Up</p>
                    <button onClick={closeModal} className="delete" aria-label="close"/>
                </header>
                <section className="modal-card-body">
                    <form onSubmit={(e) => handleSubmit(e)}>
                        <div className="field">
                            <div className="control has-icons-left has-icons-right">
                                <input
                                    onChange={(e) => handleChange(e)}
                                    value={user.displayName}
                                    name="displayName"
                                    required
                                    className="input is-info is-medium"
                                    placeholder="Your Display Name"/>
                                <span className="icon is-small is-left">
                                    <FontAwesomeIcon icon={faUserTie}/>
                                </span>
                            </div>
                        </div>
                        <div className="field">
                            <div className="control has-icons-left has-icons-right">
                                <input
                                    onChange={(e) => handleChange(e)}
                                    value={user.username}
                                    name="username"
                                    required
                                    className="input is-info is-medium"
                                    placeholder="Your Username"/>
                                <span className="icon is-small is-left">
                                    <FontAwesomeIcon icon={faUser}/>
                                </span>
                            </div>
                        </div>
                        <div className="field">
                            <div className="control has-icons-left has-icons-right">
                                <input
                                    onChange={(e) => handleChange(e)}
                                    value={user.email}
                                    name="email"
                                    type="email"
                                    required
                                    className="input is-info is-medium"
                                    placeholder="Your Email"/>
                                <span className="icon is-small is-left">
                                    <FontAwesomeIcon icon={faEnvelope}/>
                                </span>
                            </div>
                        </div>
                        <div className="field">
                            <div className="control has-icons-left has-icons-right">
                                <input
                                    onChange={(e) => handleChange(e)}
                                    value={user.password}
                                    name="password"
                                    type="password"
                                    required
                                    className="input is-info is-medium"
                                    placeholder="Your Password"/>
                                <span className="icon is-small is-left">
                                    <FontAwesomeIcon icon={faLock}/>
                                </span>
                            </div>
                        </div>
                        <button className="button is-fullwidth is-rounded is-info is-medium">
                            <span>SIGN UP</span>
                            <span className="icon is-medium">                                
                                <FontAwesomeIcon icon={faSignInAlt}/>
                            </span>
                        </button>
                    </form>
                </section>
                <footer className="modal-card-foot level has-text-centered">
                    <div className="level-item content has-text-grey">
                        <p>
                            <small>Already using Shop?</small> <strong>
                            <a onClick={() => openModal(<LoginForm/>)} className="has-text-grey">
                                Log in here.
                            </a>
                        </strong>
                        </p>
                    </div>
                </footer>
            </div>
        </div>
    );
};

export default observer(RegisterForm);