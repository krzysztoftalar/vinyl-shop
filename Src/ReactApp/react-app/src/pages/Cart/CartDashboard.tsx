﻿import React, {useContext, useEffect} from 'react';
import {observer} from "mobx-react-lite"
import {RootStoreContext} from "../../app/stores/rootStore"
import {Link} from "react-router-dom"

const CartDashboard = () => {
    const rootStore = useContext(RootStoreContext)
    const {carts, getCart, addOneToCart, removeFromCart} = rootStore.cartStore

    useEffect(() => {
        getCart()
    }, [getCart])

    useEffect(() => {

    }, [carts])

    return (
        <div className="section">
            <div className="column">
                {carts.map(cart => (
                    <div key={cart.stockId} className="level">
                        <div className="level-item">
                            <figure className="image is-128x128">
                                <img src="/assets/Duran-Duran-Rio.jpg" alt=""/>
                            </figure>
                        </div>
                        <div className="level-item">
                            <p className="title is-wrapped">
                                {cart.productName}
                            </p>
                        </div>
                        <div className="level-item">
                            <div className="columns has-text-centered is-mobile is-multiline">
                                <div className="column is-4">
                                    <button
                                        onClick={() => removeFromCart(cart, 1)}
                                        className="button"
                                        type="button"
                                    >
                                        -
                                    </button>
                                </div>
                                <div className="column is-4"
                                >
                                    {cart.quantity}
                                </div>
                                <div className="column is-4">
                                    <button
                                        onClick={() => addOneToCart(cart, 1)}
                                        className="button"
                                        type="button"
                                    >
                                        +
                                    </button>
                                </div>
                                <div className="column is-12">
                                    <button
                                        onClick={() => removeFromCart(cart, cart.quantity)}
                                        className="button has-text-danger"
                                    >
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="level-item">
                            <p>
                                {cart.price}
                            </p>
                        </div>
                    </div>
                ))}

                <div className="has-text-right">
                    <Link to="/" className="button is-text">Ey let me buy some more</Link>
                    {
                        carts.length > 0 && <Link to="/checkouts" className="button is-success">Checkout</Link>
                    }
                </div>
            </div>
        </div>
    );
};

export default observer(CartDashboard);