import React, {Fragment, useContext, useEffect} from "react"
import {observer} from "mobx-react-lite"
import "../../../node_modules/bulma/css/bulma.css"
import NavBar from "../../pages/Home/NavBar"
import {
    RouteComponentProps,
    withRouter,
    Route,
    Switch,
} from "react-router-dom"
import ProductDetails from "../../pages/Product/ProductDetails"
import "./App.css"
import AdminDashboard from "../../pages/Admin/AdminDashboard"
import ModalContainer from "../../components/ModalContainer"
import {RootStoreContext} from "../stores/rootStore"
import {ToastContainer} from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import CartDashboard from "../../pages/Cart/CartDashboard"
import CustomerDashboard from "../../pages/Customer/CustomerDashboard"
import PrivateRoute from "../../components/PrivateRoute"
import HomeDashboard from "../../pages/Home/Home"
import CheckoutDashboard from "../../pages/Checkout/CheckoutDashboard"

const App: React.FC<RouteComponentProps> = ({location}) => {
    const rootStore = useContext(RootStoreContext);
    const {token, setAppLoaded} = rootStore.commonStore
    const {getUser} = rootStore.userStore

    useEffect(() => {
        if (token) {
            getUser().finally(() => setAppLoaded())
        } else {
            setAppLoaded()
        }
    }, [getUser, token, setAppLoaded]);

    return (
        <div className="container">
            <ModalContainer/>
            <ToastContainer position="bottom-right"/>
            <NavBar/>
            <Route exact path="/" component={HomeDashboard}/>
            <Route
                path={"/(.+)"}
                render={() => (
                    <Fragment>
                        <Switch>
                            <Route path="/products/:id" component={ProductDetails}/>
                            <Route path="/admin" component={AdminDashboard}/>
                            <Route path="/carts" component={CartDashboard}/>
                            <PrivateRoute path="/customers" component={CustomerDashboard}/>
                            <PrivateRoute path="/checkouts" component={CheckoutDashboard}/>
                        </Switch>
                    </Fragment>
                )}
            />
        </div>
    )
}

export default withRouter(observer(App))
