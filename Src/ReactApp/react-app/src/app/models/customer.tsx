﻿export interface ICustomerFormValues {
    firstName: string
    lastName: string
    email: string
    phoneNumber: string
    street: string
    homeNumber: string
    city: string
    postCode: string
    country: string
}

export class CustomerFormValues implements ICustomerFormValues {
    firstName: string = ""
    lastName: string = ""
    email: string = ""
    phoneNumber: string = ""
    street: string = ""
    homeNumber: string = ""
    city: string = ""
    postCode: string = ""
    country: string = ""

    constructor(init?: ICustomerFormValues) {
        Object.assign(this, init)
    }
}