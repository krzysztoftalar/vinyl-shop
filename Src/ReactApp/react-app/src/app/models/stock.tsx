﻿export interface IStock {
    stockId: number
    productId: number
    price: number
    quantity: number
    productType: string
}

export interface IStockFormValues extends Partial<IStock> {
}

export class StockFormValues implements IStockFormValues {
    stockId: number = 0
    productId: number = 0
    price: number = 1
    quantity: number = 1
    productType: string = ""

    constructor(init?: IStockFormValues) {
        Object.assign(this, init)
    }
}