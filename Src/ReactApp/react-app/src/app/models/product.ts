import {IStock} from "./stock"

export interface IProduct {
    productId: number
    categoryId: number
    productName: string
    description?: string   
    categoryName: string
    stock: IStock[]
}

export interface IProductsEnvelope {
    products: IProduct[]
    productsCount: number
}
