﻿export interface IUser {
    displayName: string
    username: string
    token: string
}

export interface IUserFormValues {
    displayName?: string
    username?: string
    email: string
    password: string
}

export class UserFormValues implements IUserFormValues {
    displayName: string = ""
    username: string = ""
    email: string = ""
    password: string = ""

    constructor(init?: IUserFormValues) {
        Object.assign(this, init)
    }
}