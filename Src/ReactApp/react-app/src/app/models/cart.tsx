﻿export interface ICart {   
    stockId: number
    productName: string
    quantity: number
    price: string
    realPrice: number
}

export interface ICartProductsEnvelope {
    cartProducts: ICart[]
    totalValue: number
}