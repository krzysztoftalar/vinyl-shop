import axios, {AxiosResponse} from "axios"
import {IProduct, IProductsEnvelope} from "../models/product"
import {ICategory} from "../models/category"
import {IStock, IStockFormValues} from "../models/stock"
import {IUser, IUserFormValues} from "../models/user"
import {history} from "../../"
import {toast} from "react-toastify"
import {ICartProductsEnvelope} from "../models/cart"
import {ICustomerFormValues} from "../models/customer"

axios.defaults.baseURL = "http://localhost:5002/api"

const responseBody = (response: AxiosResponse) => response.data

axios.interceptors.request.use(
    config => {
        const token = window.localStorage.getItem("jwt")
        if (token) config.headers.Authorization = `Bearer ${token}`
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

axios.interceptors.response.use(undefined, error => {
    if (error.message === "Network Error" && !error.response) {
        toast.error("Network error - make sure API is running!")
    }
    const {status, data, config, headers} = error.response
    if (status === 404) {
        history.push("/notfound")
    }
    if (status === 401 && headers["www-authenticate"]) {
        window.localStorage.removeItem("jwt")
        history.push("/")
        toast.info("Please log in")
    }
    if (status === 400 && config.method === "get" && data.errors.hasOwnProperty("id")) {
        history.push("/notfound")
    }
    if (status === 500) {
        toast.error("Server error - check the terminal for more info!")
    }
    throw error.response
})

const requests = {
    get: (url: string) => axios.get(url).then(responseBody),
    post: (url: string, body: {}) => axios.post(url, body).then(responseBody),
    put: (url: string, body: {}) => axios.put(url, body).then(responseBody),
    delete: (url: string) => axios.delete(url).then(responseBody)
}

const User = {
    login: (user: IUserFormValues): Promise<IUser> => requests.post("users/login", user),
    register: (user: IUserFormValues): Promise<IUser> => requests.post("users/register", user),
    current: (): Promise<IUser> => requests.get("/users")
}

const Products = {
    list: (params: URLSearchParams): Promise<IProductsEnvelope> =>
        axios.get("/products", {params: params}).then(responseBody),
    details: (id: number) => requests.get(`/products/${id}`)
}

const Categories = {
    list: (): Promise<ICategory[]> => requests.get("/categories"),
}

const Stock = {
    list: (): Promise<IProduct[]> => requests.get("/admin/stocks"),
    create: (stock: IStockFormValues) => requests.post("/admin/stocks", stock),
    update: (stock: { stock: IStock[] }) => requests.put("/admin/stocks", stock),
    delete: (id: number) => requests.delete(`/admin/stocks/${id}`)
}

const Cart = {
    addToCart: (stockId: number, quantity: number) =>
        axios.post(`/carts/${stockId}?quantity=${quantity}`, {}, {withCredentials: true}),
    removeFromCart: (stockId: number, quantity: number) =>
        axios.put(`/carts/${stockId}?quantity=${quantity}`, {}, {withCredentials: true}),
    getCart: (): Promise<ICartProductsEnvelope> =>
        axios.get("/carts", {withCredentials: true}).then(responseBody)
}

const Customer = {
    create: (customer: ICustomerFormValues) => requests.post("/customers", customer)
}

const Checkout = {
    createPayment: (stripeToken: { stripeToken: string }) =>
        axios.post("/checkouts", stripeToken, {withCredentials: true})
}

export default {
    User,
    Products,
    Categories,
    Stock,
    Cart,
    Customer,
    Checkout
}
