﻿import {RootStore} from "./rootStore"
import {action, observable, runInAction} from "mobx"
import agent from "../api/agent"

export default class CheckoutStore {
    rootStore: RootStore

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore
    }

    @observable submitting = false

    @action createPayment = async (token: string) => {
        this.submitting = true
        try {
            await agent.Checkout.createPayment({stripeToken: token})
            runInAction("creating payment", () => {
                this.submitting = false
            })
        } catch (error) {
            runInAction("creating payment error", () => {
                this.submitting = false
            })
            console.log(error)
        }
    }
}