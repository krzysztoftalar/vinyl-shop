import {RootStore} from "./rootStore"
import {action, observable, runInAction} from "mobx"
import agent from "../api/agent"
import {ICategory} from "../models/category"

export default class CategoryStore {
    rootStore: RootStore

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore
    }

    @observable categories: ICategory[] = []
    @observable loadingInitial = false

    @action loadCategories = async () => {
        this.loadingInitial = true
        try {
            const categories: ICategory[] = await agent.Categories.list()
            runInAction("loading categories", () => {
                this.categories = categories
                this.loadingInitial = false
            })
        } catch (error) {
            runInAction("loading categories error",() => {
                this.loadingInitial = false
            })
            console.log(error)
        }
    }
}
