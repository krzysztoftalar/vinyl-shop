﻿import {RootStore} from "./rootStore"
import {action, computed, observable, runInAction} from "mobx"
import agent from "../api/agent"
import {history} from "../../index"
import {ICart} from "../models/cart"

export default class CartStore {
    rootStore: RootStore

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore
    }

    @observable cartRegistry = new Map()
    @observable cartProduct: ICart | null = null
    @observable totalValue = 0
    @observable loadingInitial = false

    @computed get carts() {
        return Array.from(this.cartRegistry.values())
    }

    @action addToCart = async (stockId: number, quantity: number) => {
        try {
            await agent.Cart.addToCart(stockId, quantity)
            history.push("/carts")
        } catch (error) {
            console.log(error)
        }
    }

    @action addOneToCart = async (cart: ICart, quantity: number) => {
        try {
            await agent.Cart.addToCart(cart.stockId, quantity)
            runInAction("adding cart", () => {
                this.cartProduct = cart
                this.cartProduct.quantity += quantity;
                this.cartRegistry.set(this.cartProduct.stockId, this.cartProduct)
                this.totalValue += cart.realPrice
            })
        } catch (error) {
            console.log(error)
        }
    }

    @action removeFromCart = async (cart: ICart, quantity: number) => {
        try {
            await agent.Cart.removeFromCart(cart.stockId, quantity)
            runInAction("deleting cart", () => {
                this.removeOneFromCart(cart, quantity)
            })
        } catch (error) {
            console.log(error)
        }
    }

    removeOneFromCart = (cart: ICart, quantity: number) => {
        this.cartProduct = cart
        this.cartProduct.quantity -= quantity;
        this.cartRegistry.set(this.cartProduct.stockId, this.cartProduct)
        this.totalValue -= cart.realPrice * quantity
        if (this.cartProduct.quantity === 0) {
            this.removeAllFromCart(cart)
        }
    }

    removeAllFromCart = (cart: ICart) => {
        this.cartProduct = null
        this.cartRegistry.delete(cart.stockId)
    }

    @action getCart = async () => {
        this.loadingInitial = true
        try {
            const cartProductsEnvelope = await agent.Cart.getCart()
            const {cartProducts, totalValue} = cartProductsEnvelope
            runInAction("loading cart", () => {
                cartProducts.forEach(cart => {
                    this.cartRegistry.set(cart.stockId, cart)
                })
                this.totalValue = totalValue
                this.loadingInitial = false
            })
        } catch (error) {
            runInAction("loading cart error", () => {
                this.loadingInitial = false
            })
            console.log(error)
        }
    }
}