﻿import {RootStore} from "./rootStore"
import {action, computed, observable, runInAction} from "mobx"
import {IUser, IUserFormValues} from "../models/user"
import agent from "../api/agent"
import {history} from "../../index"

export default class UserStore {
    rootStore: RootStore

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore
    }

    @observable user: IUser | null = null

    @computed get isLoggedIn() {
        return !!this.user
    }

    @action login = async (userValues: IUserFormValues) => {
        try {
            const user = await agent.User.login(userValues)
            runInAction(() => {
                this.user = user
            })
            this.rootStore.commonStore.setToken(user.token)
            this.rootStore.modalStore.closeModal()
            history.push("/")
        } catch (error) {
            throw error
        }
    }

    @action register = async (userValues: IUserFormValues) => {
        try {
            const user = await agent.User.register(userValues)
            this.rootStore.commonStore.setToken(user.token)
            this.rootStore.modalStore.closeModal()
            history.push("/")
        } catch (error) {
            throw error
        }
    }

    @action getUser = async () => {
        try {
            const user = await agent.User.current()
            runInAction(() => {
                this.user = user
            })
        } catch (error) {
            console.log(error)
        }
    }

    @action logout = () => {
        this.rootStore.commonStore.setToken(null)
        this.user = null
        history.push("/")
    }
}