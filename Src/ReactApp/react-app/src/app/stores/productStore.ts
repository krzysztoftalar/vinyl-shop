import {RootStore} from "./rootStore"
import {action, computed, observable, reaction, runInAction} from "mobx"
import {IProduct} from "../models/product"
import agent from "../api/agent"

const itemsPerPage = 10

export default class ProductStore {
    rootStore: RootStore

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore

        reaction(
            () => this.predicate.keys(),
            () => {
                this.pageIndex = 0
                this.productRegistry.clear()
                this.loadProducts()
            }
        )
    }

    @observable productRegistry = new Map()
    @observable predicate = new Map()
    @observable product: IProduct | null = null
    @observable productsCount = 0
    @observable pageIndex = 0
    @observable loadingInitial = false

    @action setPredicate = (predicate: string, value: string) => {
        this.predicate.clear()
        if (predicate !== "all") {
            this.predicate.set(predicate, value)
        }
    }

    @computed get axiosParams() {
        const params = new URLSearchParams()
        params.append("itemsPerPage", String(itemsPerPage))
        params.append("skip", `${this.pageIndex ? this.pageIndex * itemsPerPage : 0}`)
        this.predicate.forEach((value, key) => {
            params.append(key, value)
        })
        return params
    }

    @computed get totalPages() {
        return Math.ceil(this.productsCount / itemsPerPage)
    }

    @action setPage = (pageIndex: number) => {
        this.pageIndex = pageIndex
    }

    @computed get products() {
        return Array.from(this.productRegistry.values())
    }

    getProduct = (id: number) => {
        return this.productRegistry.get(id)
    }

    @action loadProducts = async () => {
        this.loadingInitial = true
        try {
            const productsEnvelope = await agent.Products.list(this.axiosParams)
            const {products, productsCount} = productsEnvelope
            runInAction("loading products", () => {
                products.forEach(product => {
                    this.productRegistry.set(product.productId, product)
                })
                this.productsCount = productsCount
                this.loadingInitial = false
            })
        } catch (error) {
            runInAction("loading products error", () => {
                this.loadingInitial = false
            })
            console.log(error)
        }
    }

    @action loadProduct = async (id: number) => {
        let product: IProduct = this.getProduct(id)
        if (product) {
            this.product = product
            return product
        } else {
            this.loadingInitial = true
            try {
                product = await agent.Products.details(id)
                runInAction("getting product", () => {
                    this.product = product
                    this.productRegistry.set(product.productId, product)
                    this.loadingInitial = false
                })
                return product
            } catch (error) {
                runInAction("getting product error", () => {
                    this.loadingInitial = false
                })
                console.log(error)
            }
        }
    }
}
