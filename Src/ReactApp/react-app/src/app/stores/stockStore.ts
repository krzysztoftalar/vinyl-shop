﻿import {RootStore} from "./rootStore"
import {action, computed, observable, runInAction} from "mobx"
import {IStock, IStockFormValues} from "../models/stock"
import agent from "../api/agent"
import {IProduct} from "../models/product"
import {SyntheticEvent} from "react"

export default class StockStore {
    rootStore: RootStore

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore
    }

    @observable stockProductsRegistry = new Map()
    @observable stockRegistry = new Map()
    @observable stockProduct: IProduct | null = null
    @observable loadingInitial = false
    @observable submitting = false
    @observable target = ""

    @computed get stockProducts() {
        return Array.from(this.stockProductsRegistry.values())
    }

    @action setActiveProduct = (activeIndex: number) => {
        this.stockProduct = this.stockProductsRegistry.get(activeIndex)
    }

    @action loadStock = async () => {
        this.loadingInitial = true
        try {
            const stockProducts = await agent.Stock.list()
            runInAction("loading stock", () => {
                stockProducts.forEach(product => {
                    this.stockProductsRegistry.set(product.productId, product)
                })
                this.loadingInitial = false
            })
        } catch (error) {
            runInAction("loading stock error", () => {
                this.loadingInitial = false
            })
            console.log(error)
        }
    }

    @action createStock = async (stock: IStockFormValues) => {
        try {
            const stockProduct = await agent.Stock.create(stock)
            runInAction("creating stock", () => {
                if (this.stockProduct) {
                    this.stockProduct.stock.push(stockProduct)
                    this.stockProductsRegistry.set(this.stockProduct.productId, this.stockProduct)
                }
            })
        } catch (error) {
            runInAction("creating stock error", () => {
            })
            console.log(error)
        }
    }

    @action updateStock = async (stock: IStock[], event: SyntheticEvent<HTMLButtonElement>) => {
        this.target = event.currentTarget.name
        this.submitting = true
        const stocks = stock.map(s => {
            return {
                productId: this.stockProduct!.productId,
                stockId: s.stockId,
                price: s.price,
                quantity: s.quantity,
                productType: s.productType
            }
        })

        try {
            await agent.Stock.update({stock: stocks})
            runInAction("updating stock", () => {
                if (this.stockProduct) {                  
                    this.stockProductsRegistry.set(this.stockProduct.productId, this.stockProduct)
                    this.target = ""
                    this.submitting = false
                }
            })
        } catch (error) {
            runInAction("updating stock error", () => {
                this.submitting = false
                this.target = ""
            })
            console.log(error)
        }
    }

    @action deleteStock = async (id: number, event: SyntheticEvent<HTMLButtonElement>) => {
        this.target = event.currentTarget.name
        try {
            await agent.Stock.delete(id)
            runInAction("deleting stock", () => {
                if (this.stockProduct) {
                    this.stockProduct.stock = this.stockProduct.stock.filter(x => x.stockId !== id)
                    this.stockProduct = {...this.stockProduct}
                    this.stockProductsRegistry.set(this.stockProduct.productId, this.stockProduct)
                    this.target = ""
                }
            })
        } catch (error) {
            runInAction("deleting stock error", () => {
                this.target = ""
            })
            console.log(error)
        }
    }
}