﻿import {RootStore} from "./rootStore"
import {action, observable, runInAction} from "mobx"
import {ICustomerFormValues} from "../models/customer"
import agent from "../api/agent"

export default class CustomerStore {
    rootStore: RootStore

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore
    }

    @observable customer: ICustomerFormValues | null = null
    @observable submitting = false

    @action createCustomer = async (customer: ICustomerFormValues) => {
        this.submitting = true
        try {
            await agent.Customer.create(customer)
            runInAction("creating customer", () => {
                this.submitting = false
            })
        } catch (error) {
            runInAction("creating customer error", () => {
                this.submitting = false
            })
            console.log(error)
        }
    }
}