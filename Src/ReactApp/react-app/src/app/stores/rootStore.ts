import {createContext} from "react"
import ProductStore from "./productStore"
import CategoryStore from "./categoryStore"
import StockStore from "./stockStore"
import ModalStore from "./modalStore"
import UserStore from "./userStore"
import CommonStore from "./commonStore"
import CartStore from "./cartStore"
import CustomerStore from "./customerStore"
import CheckoutStore from "./checkoutStore"

export class RootStore {
    productStore: ProductStore
    categoryStore: CategoryStore
    stockStore: StockStore
    modalStore: ModalStore
    userStore: UserStore
    commonStore: CommonStore
    cartStore: CartStore
    customerStore: CustomerStore
    checkoutStore: CheckoutStore

    constructor() {
        this.productStore = new ProductStore(this)
        this.categoryStore = new CategoryStore(this)
        this.stockStore = new StockStore(this)
        this.modalStore = new ModalStore(this)
        this.userStore = new UserStore(this)
        this.commonStore = new CommonStore(this)
        this.cartStore = new CartStore(this)
        this.customerStore = new CustomerStore(this)
        this.checkoutStore = new CheckoutStore(this)
    }
}

export const RootStoreContext = createContext(new RootStore())
