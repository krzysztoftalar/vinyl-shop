﻿import React from 'react';

interface IProps {
    changeActiveTab: (menuItem: string) => void,
    tab: { menuItem: string, component: JSX.Element }
}

const Tab: React.FC<IProps> = ({changeActiveTab, tab}) => {
    return (
        <li onClick={() => changeActiveTab(tab.menuItem)}>
            <a>
                <span>{tab.menuItem}</span>
            </a>
        </li>
    );
};

export default Tab;