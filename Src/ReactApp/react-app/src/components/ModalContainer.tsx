﻿import React, {useContext, Fragment} from "react"
import {observer} from "mobx-react-lite"
import {RootStoreContext} from "../app/stores/rootStore"

const ModalContainer = () => {
    const rootStore = useContext(RootStoreContext)
    const {modal: {body}} = rootStore.modalStore

    return (
        <Fragment>
            {body}
        </Fragment>
    )
}

export default observer(ModalContainer)