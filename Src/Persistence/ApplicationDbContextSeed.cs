﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;

namespace Persistence
{
    public class ApplicationDbContextSeed
    {
        public static async Task SeedData(ApplicationDbContext context, UserManager<AppUser> userManager)
        {
            if (!userManager.Users.Any())
            {
                var user = new AppUser
                {
                    Id = "a",
                    DisplayName = "Tom",
                    UserName = "tom",
                    Email = "tom@test.com"
                };

                await userManager.CreateAsync(user, "Pa$$w0rd");
                await context.SaveChangesAsync();
            }

            if (!context.Categories.Any())
            {
                context.Categories.AddRange(GetPreconfiguredCategories());
                await context.SaveChangesAsync();
            }

            if (!context.Products.Any())
            {
                context.Products.AddRange(GetPreconfiguredProducts());
                await context.SaveChangesAsync();
            }
        }

        private static IEnumerable<Category> GetPreconfiguredCategories()
        {
            return new List<Category>
            {
                new Category {CategoryName = "Rock"},
                new Category {CategoryName = "Rap"},
                new Category {CategoryName = "Dance"},
                new Category {CategoryName = "Reggae"},
                new Category {CategoryName = "Ambient"},
                new Category {CategoryName = "Classic"},
                new Category {CategoryName = "Trance"},
                new Category {CategoryName = "House"}
            };
        }

        private static IEnumerable<Product> GetPreconfiguredProducts()
        {
            return new List<Product>
            {
                new Product
                {
                    CategoryId = 1,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Elvis Presley - The 50 Greatest"
                },
                new Product
                {
                    CategoryId = 1,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Nirvana ‎- Bleach "
                },
                new Product
                {
                    CategoryId = 1,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Nirvana ‎- Nevermind"
                },
                new Product
                {
                    CategoryId = 1,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Richard Marx - Rush Street"
                },
                new Product
                {
                    CategoryId = 1,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "John Lennon - Imagine"
                },
                new Product
                {
                    CategoryId = 3,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Miles Davis - Bitches Brew"
                },
                new Product
                {
                    CategoryId = 4,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Bob Marley and the wailers"
                },
                new Product
                {
                    CategoryId = 5,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Solar Fields - Until We Meet The Sky"
                },
                new Product
                {
                    CategoryId = 5,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Solar Fields - Origin # 01"
                },
                new Product
                {
                    CategoryId = 5,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Carbon Based Lifeforms - ALT:01"
                },
                new Product
                {
                    CategoryId = 5,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Carbon Based Lifeforms ‎- Twentythree"
                },
                new Product
                {
                    CategoryId = 5,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Carbon Based Lifeforms ‎– Hydroponic Garden"
                },
                new Product
                {
                    CategoryId = 5,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Aes Dana ‎– Perimeters"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - In Search Of Sunrise 1"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - In Search Of Sunrise 2"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - In Search Of Sunrise 3"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - In Search Of Sunrise 4"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - In Search Of Sunrise 5"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - In Search Of Sunrise 6"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - In Search Of Sunrise 7"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - In Search Of Sunrise 8"
                },
                new Product
                {
                    CategoryId = 1,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Nightwish - Angels Fall First"
                },
                new Product
                {
                    CategoryId = 1,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Bon Jovi - Cross Road"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - Forbidden Paradise 1"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - Forbidden Paradise 2"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - Forbidden Paradise 3"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - Forbidden Paradise 4"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - Forbidden Paradise 5"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - Forbidden Paradise 6"
                },
                new Product
                {
                    CategoryId = 7,
                    Description =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
                    ProductName = "Tiesto - Forbidden Paradise 7"
                }
            };
        }
    }
}