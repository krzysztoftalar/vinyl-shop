﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasKey(c => c.CustomerId);

            builder.Property(c => c.CustomerId)
                .IsRequired();

            builder.Property(c => c.AppUserId)
                .IsRequired(false);

            builder.Property(c => c.FirstName)
                .IsRequired()
                .HasMaxLength(30);

            builder.Property(c => c.LastName)
                .IsRequired()
                .HasMaxLength(30);

            builder.Property(c => c.Email)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(c => c.PhoneNumber)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(c => c.Street)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(c => c.HomeNumber)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(c => c.City)
                .IsRequired()
                .HasMaxLength(30);

            builder.Property(c => c.PostCode)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(c => c.Country)
                .IsRequired()
                .HasMaxLength(30);

            builder.HasOne(c => c.AppUser)
                .WithOne(a => a.Customer)
                .HasForeignKey<Customer>(c => c.AppUserId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}