﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    public class OrderStockConfiguration : IEntityTypeConfiguration<OrderStock>
    {
        public void Configure(EntityTypeBuilder<OrderStock> builder)
        {
            builder.HasKey(o => new {o.OrderId, o.StockId});

            builder.HasOne(o => o.Order)
                .WithMany(o => o.OrderStocks)
                .HasForeignKey(o => o.OrderId);

            builder.HasOne(o => o.Stock)
                .WithMany(s => s.OrderStocks)
                .HasForeignKey(o => o.StockId);

            builder.Property(o => o.Quantity)
                .HasMaxLength(10)
                .IsRequired();
        }
    }
}