﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    public class StockConfiguration : IEntityTypeConfiguration<Stock>
    {
        public void Configure(EntityTypeBuilder<Stock> builder)
        {
            builder.HasKey(s => s.StockId);

            builder.Property(s => s.StockId)
                .IsRequired();
            
            builder.Property(s => s.Price)
                .IsRequired()
                .HasColumnType("decimal(18,2)");

            builder.Property(s => s.Quantity)
                .IsRequired()
                .HasMaxLength(10);

            builder.Property(s => s.ProductType)
                .IsRequired()
                .HasMaxLength(10);

            builder.HasOne(s => s.Product)
                .WithMany(p => p.Stocks)
                .HasForeignKey(s => s.ProductId);
            
           
        }
    }
}