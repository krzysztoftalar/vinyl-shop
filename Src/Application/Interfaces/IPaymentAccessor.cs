﻿using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IPaymentAccessor
    {
        Task<string> CreatePayment(string stripeToken, decimal amount);
    }
}