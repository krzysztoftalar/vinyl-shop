﻿using System;
using System.Collections.Generic;
using Application.Services.Cart;

namespace Application.Interfaces
{
    public interface ISessionManager
    {
        string GetId();
        void AddProduct(CartProduct cartProduct);
        void RemoveProduct(int stockId, int quantity);
        IEnumerable<TResult> GetCart<TResult>(Func<CartProduct, TResult> selector);
        void ClearCart();
    }
}