﻿using System.Threading.Tasks;
using Application.Services.Cart.CartReader;

namespace Application.Interfaces
{
    public interface ICartReader
    {
        Task<CartProductsEnvelope> ReadCart();
    }
}