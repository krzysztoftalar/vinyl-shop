﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Services.Customer.CreateCustomer
{
    public class CreateCustomer
    {
        public class Request : IRequest
        {
            public int CustomerId { get; set; }

            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string PhoneNumber { get; set; }

            public string Street { get; set; }
            public string HomeNumber { get; set; }
            public string City { get; set; }
            public string PostCode { get; set; }
            public string Country { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(x => x.FirstName).NotEmpty();
                RuleFor(x => x.LastName).NotEmpty();
                RuleFor(x => x.Email).NotEmpty().EmailAddress();
                RuleFor(x => x.PhoneNumber).NotEmpty();
                RuleFor(x => x.Street).NotEmpty();
                RuleFor(x => x.HomeNumber).NotEmpty();
                RuleFor(x => x.City).NotEmpty();
                RuleFor(x => x.PostCode).NotEmpty();
                RuleFor(x => x.Country).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Request>
        {
            private readonly IApplicationDbContext _context;
            private readonly IUserAccessor _userAccessor;

            public Handler(IApplicationDbContext context, IUserAccessor userAccessor)
            {
                _context = context;
                _userAccessor = userAccessor;
            }

            public async Task<Unit> Handle(Request request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x =>
                    x.UserName == _userAccessor.GetCurrentUsername(), cancellationToken: cancellationToken);

                Domain.Entities.Customer customer;

                if (user != null)
                {
                    customer = new Domain.Entities.Customer
                    {
                        CustomerId = request.CustomerId,
                        AppUserId = user.Id,

                        FirstName = request.FirstName,
                        LastName = request.LastName,
                        Email = request.Email,
                        PhoneNumber = request.PhoneNumber,

                        Street = request.Street,
                        HomeNumber = request.HomeNumber,
                        City = request.City,
                        PostCode = request.PostCode,
                        Country = request.Country
                    };
                }
                else
                {
                    customer = new Domain.Entities.Customer
                    {
                        CustomerId = request.CustomerId,

                        FirstName = request.FirstName,
                        LastName = request.LastName,
                        Email = request.Email,
                        PhoneNumber = request.PhoneNumber,

                        Street = request.Street,
                        HomeNumber = request.HomeNumber,
                        City = request.City,
                        PostCode = request.PostCode,
                        Country = request.Country
                    };
                }

                await _context.Customers.AddAsync(customer, cancellationToken);

                var success = await _context.SaveChangesAsync(cancellationToken) > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}