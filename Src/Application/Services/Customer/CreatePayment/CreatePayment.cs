﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Services.Customer.CreatePayment
{
    public class CreatePayment
    {
        public class Request : IRequest
        {
            public string StripeToken { get; set; }
        }

        public class RequestHandler : IRequestHandler<Request>
        {
            private readonly IApplicationDbContext _context;
            private readonly IPaymentAccessor _paymentAccessor;
            private readonly IUserAccessor _userAccessor;
            private readonly ISessionManager _sessionManager;
            private readonly ICartReader _cartReader;

            public RequestHandler(IApplicationDbContext context, IPaymentAccessor paymentAccessor,
                IUserAccessor userAccessor, ISessionManager sessionManager, ICartReader cartReader)
            {
                _context = context;
                _paymentAccessor = paymentAccessor;
                _userAccessor = userAccessor;
                _sessionManager = sessionManager;
                _cartReader = cartReader;
            }

            public async Task<Unit> Handle(Request request, CancellationToken cancellationToken)
            {
                var user = await _context.Users
                    .Include(x => x.Customer)
                    .SingleOrDefaultAsync(x =>
                        x.UserName == _userAccessor.GetCurrentUsername(), cancellationToken: cancellationToken);

                var cartProducts = await _cartReader.ReadCart();

                var stockOnHold = await _context.StockOnHolds
                    .Where(x => x.SessionId == _sessionManager.GetId())
                    .ToListAsync(cancellationToken);

                _context.StockOnHolds.RemoveRange(stockOnHold);

                var stripeId = await _paymentAccessor.CreatePayment(request.StripeToken, cartProducts.TotalValue);

                var order = new Order
                {
                    CustomerId = user.Customer.CustomerId,
                    OrderRef = CreateOrderReference(),
                    StripeReference = stripeId,
                    OrderStocks = cartProducts.CartProducts.Select(x => new OrderStock
                    {
                        StockId = x.StockId,
                        Quantity = x.Quantity
                    }).ToList()
                };

                await _context.Orders.AddAsync(order, cancellationToken);

                var success = await _context.SaveChangesAsync(cancellationToken) > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem creating payment");
            }

            public string CreateOrderReference()
            {
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                var result = new char[12];
                var rnd = new Random();

                do
                {
                    for (var i = 0; i < result.Length; i++)
                    {
                        result[i] = chars[rnd.Next(chars.Length)];
                    }
                } while (_context.Orders.Any(x => x.OrderRef == new string(result)));

                return new string(result);
            }
        }
    }
}