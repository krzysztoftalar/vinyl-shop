﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Application.Interfaces;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Services.Cart.AddToCart
{
    public class AddToCart
    {
        public class Request : IRequest
        {
            public int StockId { get; set; }
            public int Quantity { get; set; }
        }

        public class Handler : IRequestHandler<Request>
        {
            private readonly IApplicationDbContext _context;
            private readonly ISessionManager _sessionManager;

            public Handler(IApplicationDbContext context, ISessionManager sessionManager)
            {
                _context = context;
                _sessionManager = sessionManager;
            }

            public async Task<Unit> Handle(Request request, CancellationToken cancellationToken)
            {
                var stock = await _context.Stocks
                    .Include(x => x.Product)
                    .FirstOrDefaultAsync(x => x.StockId == request.StockId, cancellationToken: cancellationToken);

                if (stock.Quantity < request.Quantity)
                    throw new RestException(HttpStatusCode.NotFound, "Not enough in stock");

                stock.Quantity -= request.Quantity;

                var stockOnHold = await _context.StockOnHolds
                    .Where(x => x.SessionId == _sessionManager.GetId())
                    .ToListAsync(cancellationToken: cancellationToken);

                if (stockOnHold.Any(x => x.StockId == request.StockId))
                {
                    stockOnHold.Find(x => x.StockId == request.StockId).Quantity += request.Quantity;
                }
                else
                {
                    await _context.StockOnHolds.AddAsync(new StockOnHold
                    {
                        SessionId = _sessionManager.GetId(),
                        StockId = request.StockId,
                        Quantity = request.Quantity,
                        ExpiryDate = DateTime.Now.AddMinutes(20)
                    }, cancellationToken);
                }

                foreach (var s in stockOnHold)
                {
                    s.ExpiryDate = DateTime.Now.AddMinutes(20);
                }

                var cartProduct = new CartProduct
                {
                    ProductId = stock.ProductId,
                    StockId = stock.StockId,
                    ProductName = stock.Product.ProductName,
                    Quantity = request.Quantity,
                    Price = stock.Price
                };

                _sessionManager.AddProduct(cartProduct);

                var success = await _context.SaveChangesAsync(cancellationToken) > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem adding to cart");
            }
        }
    }
}