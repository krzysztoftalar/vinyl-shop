﻿using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Services.Cart.CartReader;
using MediatR;

namespace Application.Services.Cart.GetCart
{
    public class GetCart
    {
        public class Request : IRequest<CartProductsEnvelope>
        {
        }

        public class Handler : IRequestHandler<Request, CartProductsEnvelope>
        {
            private readonly ICartReader _cartReader;

            public Handler(ICartReader cartReader)
            {
                _cartReader = cartReader;
            }

            public async Task<CartProductsEnvelope> Handle(Request request, CancellationToken cancellationToken)
            {
                return await _cartReader.ReadCart();
            }
        }
    }
}