﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Services.Cart.RemoveFromCart
{
    public class RemoveFromCart
    {
        public class Request : IRequest
        {
            public int StockId { get; set; }
            public int Quantity { get; set; }
        }

        public class Handler : IRequestHandler<Request>
        {
            private readonly IApplicationDbContext _context;
            private readonly ISessionManager _sessionManager;

            public Handler(IApplicationDbContext context, ISessionManager sessionManager)
            {
                _context = context;
                _sessionManager = sessionManager;
            }

            public async Task<Unit> Handle(Request request, CancellationToken cancellationToken)
            {
                var stock = await _context.Stocks
                    .FirstOrDefaultAsync(x => x.StockId == request.StockId, cancellationToken: cancellationToken);

                var stockOnHold = await _context.StockOnHolds
                    .Where(x => x.SessionId == _sessionManager.GetId() && x.StockId == request.StockId)
                    .FirstOrDefaultAsync(cancellationToken: cancellationToken);

                stock.Quantity += request.Quantity;
                stockOnHold.Quantity -= request.Quantity;

                if (stockOnHold.Quantity <= 0)
                {
                    _context.StockOnHolds.Remove(stockOnHold);
                }

                _sessionManager.RemoveProduct(request.StockId, request.Quantity);

                var success = await _context.SaveChangesAsync(cancellationToken) > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem removing from cart");
            }
        }
    }
}