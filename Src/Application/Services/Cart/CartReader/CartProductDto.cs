﻿namespace Application.Services.Cart.CartReader
{
    public class CartProductDto 
    {
        public int StockId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public string Price { get; set; }
        public decimal RealPrice { get; set; }
    }
}