﻿using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain.Infrastructure;

namespace Application.Services.Cart.CartReader
{
    public class CartReader : ICartReader
    {
        private readonly ISessionManager _sessionManager;

        public CartReader(ISessionManager sessionManager)
        {
            _sessionManager = sessionManager;
        }

        public async Task<CartProductsEnvelope> ReadCart()
        {
            var cartProducts = await Task.Run(() =>
                _sessionManager.GetCart(x => new CartProductDto
                {
                    StockId = x.StockId,
                    ProductName = x.ProductName,
                    Quantity = x.Quantity,
                    Price = x.Price.GetValueString(),
                    RealPrice = x.Price
                }).ToList());

            return new CartProductsEnvelope
            {
                CartProducts = cartProducts,
                TotalValue = cartProducts.Sum(x => x.Quantity * x.RealPrice)
            };
        }
    }
}