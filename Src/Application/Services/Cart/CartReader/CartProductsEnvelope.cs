﻿using System.Collections.Generic;

namespace Application.Services.Cart.CartReader
{
    public class CartProductsEnvelope
    {
        public IEnumerable<CartProductDto> CartProducts { get; set; }
        public decimal TotalValue { get; set; }
    }
}