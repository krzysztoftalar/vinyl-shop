﻿using System.Collections.Generic;

namespace Application.Services.Product.GetProducts
{
    public class ProductsEnvelope
    {
        public List<ProductsDto> Products { get; set; }
        public int ProductsCount { get; set; }
    }
}