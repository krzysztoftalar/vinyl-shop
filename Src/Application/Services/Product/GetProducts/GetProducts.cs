﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Services.Product.GetProducts
{
    public class GetProducts
    {
        public class Request : IRequest<ProductsEnvelope>
        {
            public string CategoryName { get; set; }
            public int? ItemsPerPage { get; set; }
            public int? Skip { get; set; }
        }

        public class Handler : IRequestHandler<Request, ProductsEnvelope>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ProductsEnvelope> Handle(Request request, CancellationToken cancellationToken)
            {
                var queryable = _context.Products
                    .Include(x => x.Stocks)
                    .ProjectTo<ProductsDto>(_mapper.ConfigurationProvider)
                    .OrderBy(x => x.ProductName)
                    .AsNoTracking()
                    .AsQueryable();

                if (request.CategoryName != null)
                {
                    queryable = queryable
                        .Where(x => x.CategoryName == request.CategoryName);
                }

                var products = await queryable
                    .Skip(request.Skip ?? 0)
                    .Take(request.ItemsPerPage ?? 10)
                    .ToListAsync(cancellationToken: cancellationToken);

                return new ProductsEnvelope
                {
                    Products = products,
                    ProductsCount = queryable.Count()
                };
            }
        }
    }
}