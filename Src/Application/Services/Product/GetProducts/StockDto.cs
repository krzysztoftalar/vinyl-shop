﻿using Application.Mappings;
using AutoMapper;
using Domain.Entities;
using Domain.Infrastructure;

namespace Application.Services.Product.GetProducts
{
    public class StockDto : IMapFrom<Stock>
    {
        public int StockId { get; set; }
        public string Price { get; set; }
        public int Quantity { get; set; }
        public string ProductType { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Stock, StockDto>()
                .ForMember(d =>
                    d.Price, opt =>
                    opt.MapFrom(src => src.Price.GetValueString())) ;
        }
    }
}