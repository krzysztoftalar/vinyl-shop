﻿using System.Collections.Generic;
using Application.Mappings;
using AutoMapper;
using Domain.Infrastructure;

namespace Application.Services.Product.GetProducts
{
    public class ProductsDto : IMapFrom<Domain.Entities.Product>
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public IEnumerable<StockDto> Stock { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Domain.Entities.Product, ProductsDto>()
                .ForMember(d =>
                    d.CategoryName, opt =>
                    opt.MapFrom(src => src.Category.CategoryName))
                .ForMember(d =>
                    d.Stock, opt =>
                    opt.MapFrom(src => src.Stocks));
        }
    }
}