﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Services.Product.GetProduct
{
    public class GetProduct
    {
        public class Request : IRequest<ProductDto>
        {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<Request, ProductDto>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ProductDto> Handle(Request request, CancellationToken cancellationToken)
            {
                var stocksOnHold = await _context.StockOnHolds
                    .Where(x => x.ExpiryDate < DateTime.Now)
                    .ToListAsync(cancellationToken: cancellationToken);

                if (stocksOnHold.Count > 0)
                {
                    var stockToReturn = _context.Stocks
                        .AsEnumerable()
                        .Where(x => stocksOnHold.Any(y => y.StockId == x.StockId));

                    foreach (var stock in stockToReturn)
                    {
                        stock.Quantity += stocksOnHold.FirstOrDefault(x => x.StockId == stock.StockId).Quantity;
                    }

                    _context.StockOnHolds.RemoveRange(stocksOnHold);

                    await _context.SaveChangesAsync(cancellationToken);
                }

                return await _context.Products
                    .Include(x => x.Stocks)
                    .Where(x => x.ProductId == request.Id)
                    .ProjectTo<ProductDto>(_mapper.ConfigurationProvider)
                    .AsNoTracking()
                    .FirstOrDefaultAsync(cancellationToken);
            }
        }
    }
}