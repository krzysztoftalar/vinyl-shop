﻿using System.Collections.Generic;
using Application.Mappings;
using AutoMapper;
using Domain.Infrastructure;

namespace Application.Services.Product.GetProduct
{
    public class ProductDto : IMapFrom<Domain.Entities.Product>
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public IEnumerable<StockDto> Stock { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Domain.Entities.Product, ProductDto>()
                .ForMember(d =>
                    d.Stock, opt =>
                    opt.MapFrom(src => src.Stocks));
        }
    }
}