﻿using Application.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.Services.User.Register
{
    public class RegisterUserDto 
    {
        public string DisplayName { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }
}