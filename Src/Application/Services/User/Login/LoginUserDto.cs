﻿namespace Application.Services.User.Login
{
    public class LoginUserDto
    {
        public string DisplayName { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }
}