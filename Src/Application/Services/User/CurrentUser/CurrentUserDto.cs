﻿namespace Application.Services.User.CurrentUser
{
    public class CurrentUserDto
    {
        public string DisplayName { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }
}