﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Services.StockAdmin.GetStocks
{
    public class GetStock
    {
        public class Request : IRequest<List<StockProductsDto>>
        {
        }

        public class Handler : IRequestHandler<Request, List<StockProductsDto>>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<StockProductsDto>> Handle(Request request, CancellationToken cancellationToken)
            {
                var stock = await _context.Products
                    .Include(x => x.Stocks)
                    .ProjectTo<StockProductsDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken: cancellationToken);

                return stock;
            }
        }
    }
}