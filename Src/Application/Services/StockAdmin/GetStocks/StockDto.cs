﻿using Application.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.Services.StockAdmin.GetStocks
{
    public class StockDto : IMapFrom<Stock>
    {
        public int StockId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string ProductType { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Stock, StockDto>();
        }
    }
}