﻿using System.Collections.Generic;
using Application.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.Services.StockAdmin.GetStocks
{
    public class StockProductsDto : IMapFrom<Domain.Entities.Product>
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public IEnumerable<StockDto> Stock { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Domain.Entities.Product, StockProductsDto>()
                .ForMember(d =>
                    d.Stock, opt =>
                    opt.MapFrom(src => src.Stocks));
        }
    }
}