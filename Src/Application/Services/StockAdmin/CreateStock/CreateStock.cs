﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using Domain.Entities;
using FluentValidation;
using MediatR;

namespace Application.Services.StockAdmin.CreateStock
{
    public class CreateStock
    {
        public class Request : IRequest<StockDto>
        {
            public int StockId { get; set; }
            public int ProductId { get; set; }
            public decimal Price { get; set; }
            public int Quantity { get; set; }
            public string ProductType { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(x => x.Price).NotEmpty();
                RuleFor(x => x.Quantity).NotEmpty();
                RuleFor(x => x.ProductType).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Request, StockDto>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<StockDto> Handle(Request request, CancellationToken cancellationToken)
            {
                var stock = new Stock
                {
                    StockId = request.StockId,
                    ProductId = request.ProductId,
                    Price = request.Price,
                    Quantity = request.Quantity,
                    ProductType = request.ProductType
                };

                await _context.Stocks.AddAsync(stock, cancellationToken);

                var success = await _context.SaveChangesAsync(cancellationToken) > 0;

                if (success) return _mapper.Map<StockDto>(stock);

                throw new Exception("Problem saving changes");
            }
        }
    }
}