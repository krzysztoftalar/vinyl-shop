﻿using Application.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.Services.StockAdmin.UpdateStock
{
    public class StocksDto : IMapFrom<Stock>
    {
        public int StockId { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string ProductType { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Stock, StocksDto>();
        }
    }
}