﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using Domain.Entities;
using MediatR;
using Newtonsoft.Json;

namespace Application.Services.StockAdmin.UpdateStock
{
    public class UpdateStocks 
    {
        public class Request : IRequest<List<StocksDto>>
        {
            [JsonProperty("stock")]
            public List<StocksDto> Stock { get; set; }
        }

        public class Handler : IRequestHandler<Request, List<StocksDto>>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<StocksDto>> Handle(Request request, CancellationToken cancellationToken)
            {
                var stocks = new List<Stock>();

                foreach (var stock in request.Stock)
                    stocks.Add(new Stock
                    {
                        StockId = stock.StockId,
                        ProductId = stock.ProductId,
                        Price = stock.Price,
                        Quantity = stock.Quantity,
                        ProductType = stock.ProductType
                    });

                _context.Stocks.UpdateRange(stocks);

                var success = await _context.SaveChangesAsync(cancellationToken) > 0;

                if (success) return _mapper.Map<List<StocksDto>>(stocks);

                throw new Exception("Problem saving changes");
            }
        }
    }
}