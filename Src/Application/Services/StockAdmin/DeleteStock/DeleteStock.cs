﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using MediatR;

namespace Application.Services.StockAdmin.DeleteStock
{
    public class DeleteStock
    {
        public class Request : IRequest
        {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<Request>
        {
            private readonly IApplicationDbContext _context;

            public Handler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Request request, CancellationToken cancellationToken)
            {
                var stock = await _context.Stocks.FindAsync(request.Id);

                _context.Stocks.Remove(stock);
                
                var success = await _context.SaveChangesAsync(cancellationToken) > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}