﻿using Application.Mappings;
using AutoMapper;

namespace Application.Services.Category
{
    public class CategoryDto : IMapFrom<Domain.Entities.Category>
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Domain.Entities.Category, CategoryDto>();
        }
    }
}