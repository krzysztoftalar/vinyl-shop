﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Services.Category
{
    public class GetCategories
    {
        public class Request : IRequest<List<CategoryDto>>
        {
        }

        public class Handler : IRequestHandler<Request, List<CategoryDto>>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public Handler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<CategoryDto>> Handle(Request request, CancellationToken cancellationToken)
            {
                return await _context.Categories
                    .ProjectTo<CategoryDto>(_mapper.ConfigurationProvider)
                    .OrderBy(x => x.CategoryName)
                    .ToListAsync(cancellationToken);
            }
        }
    }
}