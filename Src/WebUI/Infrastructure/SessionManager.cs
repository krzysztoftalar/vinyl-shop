﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interfaces;
using Application.Services.Cart;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace WebUI.Infrastructure
{
    public class SessionManager : ISessionManager
    {
        private readonly ISession _session;
        private const string KeyCart = "carts";

        public SessionManager(IHttpContextAccessor httpContextAccessor)
        {
            _session = httpContextAccessor.HttpContext.Session;
        }

        public string GetId()
        {
            return _session.Id;
        }

        public void AddProduct(CartProduct cartProduct)
        {
            var cartList = new List<CartProduct>();
            var stringObject = _session.GetString(KeyCart);

            if (!string.IsNullOrEmpty(stringObject))
            {
                cartList = JsonConvert.DeserializeObject<List<CartProduct>>(stringObject);
            }

            if (cartList.Any(x => x.StockId == cartProduct.StockId))
            {
                cartList.Find(x => x.StockId == cartProduct.StockId).Quantity += cartProduct.Quantity;
            }
            else
            {
                cartList.Add(cartProduct);
            }

            stringObject = JsonConvert.SerializeObject(cartList);

            _session.SetString(KeyCart, stringObject);
        }

        public void RemoveProduct(int stockId, int quantity)
        {
            var cartList = new List<CartProduct>();
            var stringObject = _session.GetString(KeyCart);
            
            if (!string.IsNullOrEmpty(stringObject))
            {
                cartList = JsonConvert.DeserializeObject<List<CartProduct>>(stringObject);
            }

            var product = cartList.Find(x => x.StockId == stockId);
            product.Quantity -= quantity;

            if (product.Quantity <= 0) cartList.Remove(product);
            
            stringObject = JsonConvert.SerializeObject(cartList);

            _session.SetString(KeyCart, stringObject);
        }

        public IEnumerable<TResult> GetCart<TResult>(Func<CartProduct, TResult> selector)
        {
            var stringObject = _session.GetString("carts");

            if (string.IsNullOrEmpty(stringObject)) return new List<TResult>();

            var cartList = JsonConvert.DeserializeObject<IEnumerable<CartProduct>>(stringObject);

            return cartList.Select(selector);
        }
        
        public void ClearCart()
        {
            _session.Remove(KeyCart);
        }
    }
}