using System;
using System.Text;
using Application;
using Application.Interfaces;
using Application.Services.Cart.CartReader;
using Application.Services.Customer.CreateCustomer;
using Application.Services.StockAdmin.CreateStock;
using Application.Services.User.Login;
using Application.Services.User.Register;
using FluentValidation.AspNetCore;
using Infrastructure;
using Infrastructure.Payment;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Persistence;
using Stripe;
using WebUI.Configuration;
using WebUI.Infrastructure;
using WebUI.Middleware;

namespace WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options =>
                {
                    var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                    options.Filters.Add(new AuthorizeFilter(policy));
                })
                .AddFluentValidation(options =>
                {
                    options.RegisterValidatorsFromAssemblyContaining<CreateStock>();
                    options.RegisterValidatorsFromAssemblyContaining<CreateCustomer>();
                    options.RegisterValidatorsFromAssemblyContaining<Register>();
                    options.RegisterValidatorsFromAssemblyContaining<Login>();
                })
                .AddNewtonsoftJson();

            services.AddPersistence(Configuration);
            services.AddApplication();
            services.AddInfrastructure();

            services.AddHealthChecks().AddDbContextCheck<ApplicationDbContext>();

            services.AddSwaggerDocumentation();

            var key = new SymmetricSecurityKey(Encoding.UTF8
                .GetBytes(Configuration.GetValue<string>("TokenKey:SecurityKey")));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = key,
                        ValidateAudience = false,
                        ValidateIssuer = false,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };
                });

            services.AddCors(options =>
                options.AddPolicy("CorsPolicy",
                    policy =>
                    {
                        policy
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .WithOrigins("http://localhost:3000")
                            .AllowCredentials()
                            .WithExposedHeaders("WWW-Authenticate");
                    }
                ));

            services.AddSession(options =>
            {
                options.Cookie.Name = "Carts";
                options.Cookie.MaxAge = TimeSpan.FromMinutes(20);
            });
            
            services.AddScoped<ISessionManager, SessionManager>();
            services.AddScoped<ICartReader, CartReader>();
            StripeConfiguration.ApiKey = Configuration.GetSection("Stripe")["SecretKey"];
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<ErrorHandlingMiddleware>();

            app.UseRouting();
            app.UseCors("CorsPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSession();

            app.UseSwaggerDocumentation();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}