﻿using System.Threading.Tasks;
using Application.Services.User.CurrentUser;
using Application.Services.User.Login;
using Application.Services.User.Register;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    public class UsersController : BaseController
    {
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<LoginUserDto>> Login(Login.Request request)
        {
            return await Mediator.Send(request);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<ActionResult<RegisterUserDto>> Register(Register.Request request)
        {
            return await Mediator.Send(request);
        }

        [HttpGet]
        public async Task<ActionResult<CurrentUserDto>> CurrentUser()
        {
            return await Mediator.Send(new CurrentUser.Request());
        }
    }
}