﻿using System.Threading.Tasks;
using Application.Services.Customer.CreatePayment;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    public class CheckoutsController : BaseController
    {
        [HttpPost]
        public async Task<ActionResult<Unit>> CreatePayment([FromBody] CreatePayment.Request request)
        {
            return await Mediator.Send(request);
        }
    }
}