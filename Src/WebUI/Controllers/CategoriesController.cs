﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Services.Category;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    public class CategoriesController : BaseController
    {
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<List<CategoryDto>>> GetProducts()
        {
            return await Mediator.Send(new GetCategories.Request());
        }
    }
}