﻿using System.Threading.Tasks;
using Application.Services.Product.GetProduct;
using Application.Services.Product.GetProducts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProductDto = Application.Services.Product.GetProduct.ProductDto;

namespace WebUI.Controllers
{
    [AllowAnonymous]
    public class ProductsController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<ProductsEnvelope>> GetProducts(string categoryName, int? itemsPerPage, int? skip)
        {
            return await Mediator.Send(new GetProducts.Request
                {CategoryName = categoryName, ItemsPerPage = itemsPerPage, Skip = skip});
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductDto>> GetProduct(int id)
        {
            return await Mediator.Send(new GetProduct.Request {Id = id});
        }
    }
}