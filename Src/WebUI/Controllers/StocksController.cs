﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Services.StockAdmin.CreateStock;
using Application.Services.StockAdmin.DeleteStock;
using Application.Services.StockAdmin.GetStocks;
using Application.Services.StockAdmin.UpdateStock;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StockDto = Application.Services.StockAdmin.CreateStock.StockDto;

namespace WebUI.Controllers
{
    [AllowAnonymous]
    [Route("api/Admin/[controller]")]
    public class StocksController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<List<StockProductsDto>>> GetStock()
        {
            return await Mediator.Send(new GetStock.Request());
        }

        [HttpPost]
        public async Task<ActionResult<StockDto>> CreateStock(CreateStock.Request request)
        {
            return await Mediator.Send(request);
        }

        [HttpPut]
        public async Task<ActionResult<List<StocksDto>>> UpdateStocks([FromBody] UpdateStocks.Request request)
        {
            return await Mediator.Send(request);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> DeleteStock(int id)
        {
            return await Mediator.Send(new DeleteStock.Request {Id = id});
        }
    }
}