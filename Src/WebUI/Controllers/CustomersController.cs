﻿using System.Threading.Tasks;
using Application.Services.Customer.CreateCustomer;
using Application.Services.Customer.CreatePayment;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    public class CustomersController : BaseController
    {
        [HttpPost]
        public async Task<ActionResult<Unit>> CreateCustomer(CreateCustomer.Request request)
        {
            return await Mediator.Send(request);
        }
    }
}