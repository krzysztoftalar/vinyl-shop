﻿using System.Threading.Tasks;
using Application.Services.Cart.AddToCart;
using Application.Services.Cart.CartReader;
using Application.Services.Cart.GetCart;
using Application.Services.Cart.RemoveFromCart;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    [AllowAnonymous]
    public class CartsController : BaseController
    {
        [HttpPost("{stockId}")]
        public async Task<ActionResult<Unit>> AddToCart(int stockId, int quantity)
        {
            return await Mediator.Send(new AddToCart.Request {StockId = stockId, Quantity = quantity});
        }

        [HttpPut("{stockId}")]
        public async Task<ActionResult<Unit>> RemoveFromCart(int stockId, int quantity)
        {
            return await Mediator.Send(new RemoveFromCart.Request {StockId = stockId, Quantity = quantity});
        }

        [HttpGet]
        public async Task<CartProductsEnvelope> GetCart()
        {
            return await Mediator.Send(new GetCart.Request());
        }
    }
}