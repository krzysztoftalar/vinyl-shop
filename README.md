<h1 align="center">Vinyl Shop</h1>

<p align="center">
<img src="https://img.shields.io/badge/made%20by-krzysztoftalar-blue.svg" />

<img src="https://img.shields.io/badge/.NET%20Core-3.1.0-blueviolet" />

<img src="https://img.shields.io/badge/React-Hooks-blue" />

<img src="https://img.shields.io/badge/license-MIT-green" />
</p>

## About The Project

_Project in progress..._

## Features

## Built With

| Server                                                                                       | Client                                        |
| -------------------------------------------------------------------------------------------- | --------------------------------------------- |
| [.NET Core](https://docs.microsoft.com/en-us/dotnet/) 3.1.0                                  | [React](https://reactjs.org/)                 |
| [ASP.NET Web API](https://docs.microsoft.com/en-us/aspnet/core/web-api/?view=aspnetcore-3.1) | [MobX](https://mobx.js.org/)                  |
| [Entity Framework](https://docs.microsoft.com/en-us/ef/)                                     | [TypeScript](https://www.typescriptlang.org/) |
| [MediatR](https://github.com/jbogard/MediatR/wiki)                                           | [Bulma](https://bulma.io/)                    |
| [AutoMapper](https://automapper.org/)                                                        |
| [Swagger](https://swagger.io/)                                                               |

## Getting Started

### Prerequisites

- .NET Core 3.1.0
- SQL Server
- Node.js

### Installation

## License

This project is licensed under the MIT License.

## Contact

**Krzysztof Talar** - [Linkedin](https://www.linkedin.com/in/ktalar/) - krzysztoftalar@protonmail.com
